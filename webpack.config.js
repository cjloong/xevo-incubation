/**
 * This webpack configuration listens to components and apps of the system.
 */
const path = require('path');
const webpack = require("webpack");
const WebpackNotifierPlugin = require("webpack-notifier");

//Components
module.exports = [ {
	entry : {
		"qunit.proxy" : "./src/proxy/qunit.proxy.js"
		, "qunit.shell" : "./src/shell/qunit.shell.js"
	},
	devtool : '#source-map',
	output : {
		path : './bundles',
		filename : "[name].bundle.js",
		sourcePrefix : ""
	},
	plugins : [
		new WebpackNotifierPlugin()
	],
	module : {
// 		loaders : [ {
// 			test : /\.js$/,
// 			exclude : /(node_modules|bower_components)/,
// 			loader : 'babel-loader',
// 			query : {
// 				presets : [ 'es2015' ]
// 			}
// 		}


// 		]
	}
} ];