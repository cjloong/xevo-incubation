class A {

	constructor() {
		this._id = 1;
		this._name="Unknown";
	}
	
	set id(newval) {
		this._id = newval;
	}
	get id() {
		return this._id;
	}
	set name(newval) {
		this._name = newval;
	}
	get name() {
		return this._name;
	}

}

function mandatory(name) {throw new `[${name}] is a mandatory argument`};

class Memento {
	constructor() {
		this.history = [];
	}

	register(obj=mandatory(obj)) {
		var history = [];
		let proxy =  new Proxy(obj, {
			set: function(obj, prop, val) {
				console.debug(`${obj}, ${prop}, ${val}`);
				obj[prop]=val;

				history.push({obj, prop, val});

				return true;
			}
		});

		proxy.printHistory=function() {
			console.log(history);
		}

		return proxy;

	}
}
QUnit.module("Proxy", (hooks)=>{
	QUnit.module("_Development",(hooks)=>{
		hooks.beforeEach((assert) => {});
		hooks.afterEach((assert) => {});

		QUnit.skip("History proxy", assert=>{
			let a = new A();
			let memento = new Memento();
			let caretakerA = memento.register(a);

			caretakerA.newid=100;

		});
		QUnit.test("Warming up - without proxy", (assert)=>{
			let a = new A();
			assert.ok(a.id===1, `${a.id}`);
			assert.ok(a.name=="Unknown", `${a.name}`);
		});
		QUnit.test("Warming up - with proxy", (assert)=>{
			let a = new A();
			let proxya = new Proxy(a,{
				set: function(obj, prop, val) {
					console.debug(`${obj}, ${prop}, ${val}`);
					obj[prop]=val;

					return true;
				}
			});
			
			proxya.id=100;
			assert.ok(proxya.id===100);
		});

	});
});	
