const Terminal = require("xterm");
require("css!./xterm.css");
var $ = require('jquery');
require("script-loader!./sdether-josh/js/killring.js");
require("script-loader!./sdether-josh/js/history.js");

QUnit.module("Shell", (hooks)=>{
	
	QUnit.module("_Development",(hooks)=>{
		hooks.beforeEach((assert) => {
			var elem = document.createElement("div");
			elem.innerHTML = "<h1>Terminal</h1><pre id='term' class='terminaljs'></pre>"
			var ui = document.querySelector("#ui")
			ui.appendChild(elem);

		});
		hooks.afterEach((assert) => {});

		QUnit.test("JoshTerm", assert=>{
			console.log(Josh);
			debugger
		});

		QUnit.test("Xterm", assert=>{
			var buffer = [];
			var term = new Terminal();
			term.open(document.getElementById('term'));
			term.write("$ ")
			term.on('data', (data)=>{
				if(data!="\r") {
					term.write(data);
					buffer.push(data);
				} 
				else {
					term.writeln("");
					var command = buffer.join("");
					buffer = [];
					term.writeln("Exceuting:" + command);
					term.write(" $ ")
				}
			});
		});
	});
});	
